'use strict';

const $ = require('jquery');

//import '../styles/main.scss';
import '../images/photo_preloader.gif'

// Application initialization
$(() => {
    $('.j-appVersion').html(':)');

    $.ajaxSetup({ cache: true });
});
