const path = require('path');
const autoprefixer = require('autoprefixer');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');


const { merge } = require('webpack-merge'); 

const commonConfig = require('./config/webpack.common'); 

module.exports = (env) => {
    const config = require('./config/webpack.' + env); 
    return merge(commonConfig, config); 
};

