const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: "production",

  output: {
    filename: "bundle.min.js",
    path: path.resolve("dist"),
  },

  module: {
    rules: [
      {
        test: /\.gif/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outPath: "./dist",
              useRelativePath: true,
            },
          },
          {
            loader: "image-webpack-loader",
            options: {
              mozjpeg: {
                progressive: true,
                quality: 70,
              },
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "bundle.min.css",
    }),

    new CopyPlugin([{ from: "src/index.html" }]),
  ],
};
