const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "development",
  devtool: "source-map",

  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "index.js",
  },

  plugins: [new HtmlWebpackPlugin()],
};
